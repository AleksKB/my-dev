package algorithms.recursion.mergesort;

public class MergeRunner {
	public static void main(String[] args) {
		int[] numbers = {2, 7, 23, 3, 95, 14, 34, 1, 23, 51, 100, 62};
		//int[] numbers = {2, 5, 8, 23, 7, 9, 12, 16};
		
		MergeSort merge = new MergeSort();
		numbers  = merge.sort(numbers);
		
		for( int i : numbers ) {
			System.out.println(i);
		}
	}
}
