package algorithms.recursion.mergesort;

public class MergeSort {	
	public MergeSort() {		
	}
	
	public int[] sort(int[] numbers) {
		
		numbers = mergeSort(numbers, 0, numbers.length-1);
		
		return numbers;
	}
	
	private int [] mergeSort(int[] numbers, int start, int end) {
		
		if( start < end ) {
			int middle = (int) Math.floor((start + end) / 2);
			mergeSort(numbers, start, middle);
			mergeSort(numbers, middle+1, end);
			merge(numbers, start, middle, end);			
		}
		
		return numbers;
	}
	
	private int[] merge( int[] numbers, int start, int middle, int end ) {
				
		int[] leftArray = new int[middle - start+1];
		int[] rightArray = new int[end - middle];
		
		for( int i = 0; i < leftArray.length; i++ ) {			
			leftArray[i] = numbers[start+i];							
		}
		
		for( int j = 0; j < rightArray.length; j++ ) {
			rightArray[j] = numbers[middle+j+1];
		}
		
		int i = 0;
		int j = 0;
		
		for( int k = start; k <= end; k++ ) {
			if( i+start <= middle && j+middle < end ) {
				if( leftArray[i] <= rightArray[j] ) {
					numbers[k] = leftArray[i];
					i++;
				} else {
					numbers[k] = rightArray[j];
					j++;
				}
			} else if( i+start > middle ){
				numbers[k] = rightArray[j];
				j++;
			} else {
				numbers[k] = leftArray[i];
				i++;
			}
		}		
		
		return numbers;
	}
	
}
