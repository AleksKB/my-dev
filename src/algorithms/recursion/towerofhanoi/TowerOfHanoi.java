package algorithms.recursion.towerofhanoi;

public class TowerOfHanoi {

	public static void main(String[] args) {
		TowerOfHanoi toh = new TowerOfHanoi();
		toh.move(4, 'A', 'C', 'B');
	}

	public void move(int numberOfDisks, char from, char to, char inter) {
		if( numberOfDisks == 1) {
			System.out.println("Moving disc 1 from " + from + " to " + to);
		} else {
			move(numberOfDisks - 1, from, inter, to);
			System.out.println("Moving disc " + numberOfDisks + " from " + from + " to " + to);
			move(numberOfDisks - 1, inter, to, from);
		}
	}
}
