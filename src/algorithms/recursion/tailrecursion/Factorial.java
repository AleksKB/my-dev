package algorithms.recursion.tailrecursion;

public class Factorial {
	public static void main(String[] args) {
		System.out.println(factorial(4));
	}
	
	public static int factorial(int n) {
		return factorial(n,1);
	}
	
	/*
	 * has a 'accumulator' in the form of the 'result' int. this means it keeps 
	 * track of the calculation as it goes through the stack instead of calculating 
	 * as it comes back through.
	 */	
	public static int factorial( int n, int result ) {
		if( n==0 ) return result;
		
		return factorial( n-1, n*result );
	}
}
