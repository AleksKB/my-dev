package algorithms.sorting;

public class Sort {
	@SuppressWarnings("unused")
	private static int[] numbers;

	protected Sort(){	
	}
	
	public int[] swap(int[] numbers, int x, int y){
		int temp = numbers[x];
		numbers[x] = numbers[y];
		numbers[y] = temp;
		
		return numbers;
	}
}
