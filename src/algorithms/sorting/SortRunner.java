package algorithms.sorting;

import algorithms.sorting.simple.BubbleSort;
import algorithms.sorting.simple.InsertionSort;
import algorithms.sorting.simple.SelectionSort;

public class SortRunner {

	public static void main(String[] args) {
		System.out.println("Starting");
		
		int [] numbers = {2,5,4,7,8,6,1,9,3};
		//int [] numbers = {9,8,7,6,5,4,3,2,1};
		
		BubbleSort bubbleSort = new BubbleSort();
		SelectionSort selectionSort = new SelectionSort();
		InsertionSort insertionSort = new InsertionSort();
		
		numbers = insertionSort.insertionSort(numbers);
		
		printArray(numbers);
	}
	
	public static void printArray(int[] numbers){
		for( int i : numbers ){
			System.out.println(i);
		}
	}
}
