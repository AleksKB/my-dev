package algorithms.sorting.simple;

import algorithms.sorting.Sort;

public class InsertionSort extends Sort {
	public InsertionSort(){
	}
	
	public InsertionSort(int[] numbers){	
		numbers = insertionSort(numbers);
	}
	
	public int[] insertionSort(int[] numbers){
		
		for( int i = 1; i < numbers.length; i++){
			for( int j = i; j > 0; j--){
				if( numbers[j] < numbers[j-1] ){
					numbers = swap(numbers, j, j-1);
				}
			}
		}		
		
		return numbers;
	}
}
