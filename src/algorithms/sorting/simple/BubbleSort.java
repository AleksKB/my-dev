package algorithms.sorting.simple;

import algorithms.sorting.Sort;

public class BubbleSort extends Sort {
	public BubbleSort() {		
	}
	
	public BubbleSort(int[] numbers){	
		numbers = bubbleSort(numbers);
	}
	
	public int[] bubbleSort(int[] numbers){
		
		for( int i = 0; i < numbers.length-2; i++ ){
			for( int j = 0; j < numbers.length-2-i; j++ ){
				if( numbers[j] > numbers[j+1] ){
					numbers = swap(numbers, j+1, j);
				}
			}
		}
		
		return numbers;
	}
}
