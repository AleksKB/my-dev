package algorithms.sorting.simple;

import algorithms.sorting.Sort;

public class SelectionSort extends Sort {
	public SelectionSort(){	
	}
	
	public SelectionSort(int[] numbers){	
		numbers = selectionSort(numbers);
	}
		
	public int[] selectionSort(int[] numbers){
		
		int lowestPos;
		
		for( int i = 0; i < numbers.length; i++){
			lowestPos = i;
			
			for( int j = i+1; j < numbers.length - 1; j++){
				if( numbers[j] < numbers[lowestPos] ){
					lowestPos = j;
				}		
			}
			
			numbers = swap(numbers, lowestPos, i);
		}
		
		return numbers;
	}
}
