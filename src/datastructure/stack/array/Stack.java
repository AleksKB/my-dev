package datastructure.stack.array;

public class Stack {
	private int maxSize = 9;
	private int stack[] = new int[maxSize];
	private int stackPos = 0;
		
	/*
	 * stacks are First in, Last Out
	 * This is a stack implementation using an array.
	 */
	
	//removes top element
	public void pop(){
		if( stackPos > 0 ){
			stackPos--;
		}		
	}
	
	//looks at the value of the top element
	public int peek(){
		return stack[stackPos];
	}
	
	//adds a new value to the stack
	public void push(int i){
		stackPos++;
		stack[stackPos] = i; 
	}
	
	public int getStackPos(){
		return stackPos;
	}
}