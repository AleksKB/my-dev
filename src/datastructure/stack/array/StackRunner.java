package datastructure.stack.array;

public class StackRunner {

	public static void main(String[] args) {
		Stack stack = new Stack();
		
		stack.push(2);
		stack.push(3);
		stack.push(6);
		stack.push(8);
				
		System.out.println("Stack Item [" + stack.getStackPos() + "] = " + stack.peek());
		
		stack.pop();
		
		System.out.println("Stack Item [" + stack.getStackPos() + "] = " + stack.peek());
	}

}
