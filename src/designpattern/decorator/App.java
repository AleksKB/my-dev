package designpattern.decorator;

/*
 * Interface to create an app
 */
public interface App {
	/*
     * This method creates an app for our app store. Seems silly and broken by design to 
     * have the App develop itself but for this example it fits the purpose.
     */
    void developApp();
}
