package designpattern.decorator;

public class IOSApp implements App {

	@Override
	public void developApp() {
		System.out.println("Developing an iOS app");
	}

}
