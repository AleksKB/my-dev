package designpattern.decorator;

/*
 * The Decorator pattern allows you to add additional behaviour to an individual object dynamically
 * without affecting the behaviour of other objects of the same class.
 * 
 * It's used in Java's I/O streams implementation. 
 */
public class DecoratorExample {
	public static void main(String... args) {
        final App tvApp = new TVAppDecorator(new IOSApp());
        tvApp.developApp();
        
        System.out.println("------");
        
        final App watchApp = new WatchAppDecorator(new IOSApp());
        watchApp.developApp();
        
        System.out.println("------");
        
        final App perfectApp = new SimpleDecorator(new IOSApp());
        perfectApp.developApp();
    }
}
