package designpattern.decorator;

import designpattern.decorator.AppDecorator;

/**
 * This implementation of the decorator adds a Watch extension to the provided app.
 */
public class WatchAppDecorator extends AppDecorator {
	
	private final App delegate;
	
    /**
     * A required constructor to set the delegate for this app.
     *
     * @param delegate
     *            the delegate which should be decorated.
     */
    public WatchAppDecorator(App delegate) {
        this.delegate = delegate;
    }
    @Override
    public void developApp() {
        this.delegate.developApp();
        System.out.println("Adding Watch extension...");
    }
}
