package designpattern.decorator;

/*
 * This creates a decorated App with TV extension.
 */
public class TVAppDecorator extends AppDecorator {
	
	private final App delegate;
	
	/**
     * A required constructor to set the delegate for this app.
     * 
     * @param delegate
     *            the delegate which should be decorated.
     */
    public TVAppDecorator(App delegate) {
        this.delegate = delegate;
    }
    
    @Override
    public void developApp() {
        this.delegate.developApp();
        System.out.println("Adding TV extension...");
    }
}