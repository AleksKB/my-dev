package designpattern.decorator;

/*
 * This is the basic implementation for our decorator. It is abstract so it does not need to implement the contract
 * method "developApp()".
 *
 * However it has the delegate which will be decorated later.
 */
public abstract class AppDecorator implements App {

	@Override
	public void developApp() {
		/**
	     * This is the delegate to decorate. It is package-private so the subclasses can access it so we do not need
	     * getters/setters and a constructor. However I would add one if I would write this decorator in my daily job.
	     */
	    App delegate;
	}
}