package designpattern.composite;

/**
 * This is the main entry point of the application.
 * 
 * The Composite design pattern is a structural pattern which is useful in cases where you 
 * need a tree-like hierarchy. It allows for aggregation of data across the hierarchy and 
 * allows you to treat composite and individual objects uniformly. 
 * 
 * The example below shows how a hierarchy can be created and navigated to the correct point 
 * using the 'estimateProject' method.
 *
 * @author GHajba
 *
 */
public class CompositeExample {
	
    public static void main(String... args) {
        final Developer d1 = new Developer("Jack");
        final Developer d2 = new Developer("Jill");
        final Developer d3 = new Developer("Brian");
        final Developer d4 = new Developer("Bob");
        final Manager m1 = new TeamLeader("Marc");
        final Manager m2 = new TeamLeader("Christian");
        final Manager m3 = new TeamLeader("Phil");
        
        m1.add(d3);
        m1.add(d2);
        m2.add(d1);
        m3.add(d4);
        
        final Manager m4 = new SeniorManager("Harald");
        final Manager m5 = new SeniorManager("Klaus");
        
        m4.add(m3);
        m4.add(m2);
        m5.add(m1);
        
        final VicePresident vp = new VicePresident("Joseph");
        
        vp.add(m4);
        vp.add(m5);
        
        System.out.println("Our estimate is: " + vp.estimateProject("New exotic feature"));
    }	
}
