package designpattern.composite;

/**
 * Simple implementation of a Team Leader
 *
 * @author GHajba
 */
public class TeamLeader extends Manager {
    public TeamLeader(String name) {
        super(name);
    }
    @Override
    public String toString() {
        return "I am " + getName() + ", Team Leader";
    }
}
