package designpattern.composite;

/**
 * Simple implementation of a Senior Manager
 *
 * @author GHajba
 */
public class SeniorManager extends Manager {
    public SeniorManager(String name) {
        super(name);
    }
    /**
     * Senior Managers add 10% to the estimate of the team.
     */
    @Override
    public int estimateProject(String projectDescription) {
        return Math.toIntExact(Math.round(super.estimateProject(projectDescription) * 1.1));
    }
    @Override
    public String toString() {
        return "I am " + getName() + ", Senior Manager";
    }
}
