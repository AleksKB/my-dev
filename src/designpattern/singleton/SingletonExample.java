package designpattern.singleton;

import java.io.Serializable;

/*
 * Info from Implementation 6 of the Singleton Pattern at
 * https://javabeginnerstutorial.com/design-pattern/singleton-design-pattern-java/
 * 
 * The singleton pattern ensures that there is only ever one instance of a class 
 * This can be useful in cases such as creating a logger class where there is a 
 * one way flow of information.
 * 
 * 
 * When to use a Singleton Pattern and not a static class: https://softwareengineering.stackexchange.com/a/235528 * 
 * A Singleton becomes useful when you want an actual object with its own internal state and want to only have
 * one instance of that object. This can be useful if you have a shared resource such as a logger, database, 
 * an in-memory cache, or a robotic arm. Many parts of the program could want to use this resource and you want 
 * to control their access to the resource through a single point. 
 * 
 * You could use a static class when you want to collect related pieces of functionality, but you don't need 
 * to have any internal state in any object like the servletUtils class. Another example is the Math class which 
 * contains a whole bunch of related functions that are accessed outside the context of any specific object instance.
 */

class SingletonExample implements Cloneable, Serializable {
	
	/*
	 * prevent Cloning of this class
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new CloneNotSupportedException();
	}
	
	/*
	 * handle Serialization of this class
	 */
	protected Object readResolve() {
		return createInstance();
	}
		
	/*
	 * Have a default private constructor so this object cannot be created 
	 * directly except by using the static method of this class
	 */
	private SingletonExample() {		
	}
	
	/*
	 * Have a static inner class instead of a static variable which allows
	 * for lazy initialization of the class  
	 */
	private static class LazyInit {
		private static final SingletonExample instance = new SingletonExample();
	}
	
	/*
	 * This method gets the instance of SingletonExample by checking if 
	 * it exists and creating one if it doesnt
	 * 
	 * It avoids use of 'synchronized' keyword since this can cause 
	 * performance issues in multithreaded applications
	 */
	static SingletonExample createInstance() {
		return LazyInit.instance;
	}
}
