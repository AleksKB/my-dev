package designpattern.singleton;

import java.io.Serializable;

public class Logger implements Cloneable, Serializable {
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new CloneNotSupportedException();
	}
	
	protected Object readResolve() {
		return createInstance();
	}
	
	private Logger() {		
	}
	
	private static class LazyInit {
		private static final Logger instance = new Logger();
	}
	
	static Logger createInstance() {
		return LazyInit.instance;
	}
	
	public static void write(String input) {
		System.out.println(input);
	}
}
