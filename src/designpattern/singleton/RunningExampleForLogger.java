package designpattern.singleton;

public class RunningExampleForLogger {

	public static void main(String[] args) {
		/* 
		 * you can use the logger without having to instantiate it!
		 */
		Logger.write("Here is my test print");
		Logger.write("Everything runs successfully");
		
		/*
		 * you can call the Logger's method to createInstance if you want but theres no point...
		 */
		Logger.createInstance();
		
		Logger.write("It functions the same...");
		
		/* 
		 * the following wont work 
		 */
		//Logger logger = new Logger();		
	}

}
