package designpattern.factory;

/*
 * Factory classes are useful for creating concrete implementations of objects. 
 * They keep the code flow clean, readable, and easier to maintain meaning changes
 * are easier to implement. In the example below it would be easy to add a new shape
 * type if its needed and its also easy to find where it needs to be added. 
 * 
 * Factory classes define an interface for creating an object but lets subclasses
 * decide on which to instantiate.
 */
public class FactoryExample {
	public static void main(String[] args) {
		/*
		 * Move the work of creating a new shape to the Shape Factory. 
		 */
		Shape shape = new ShapeFactory().createShape(ShapeType.SQUARE);
		shape.draw();
	}
}
