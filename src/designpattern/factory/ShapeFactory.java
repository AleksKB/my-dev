package designpattern.factory;

public class ShapeFactory {
	public Shape createShape(ShapeType type) {
		Shape shape;
		
		switch(type) {
			case CIRCLE:
				shape = new Circle();
				break;
			case SQUARE:
				shape = new Square();
				break;
			case TRIANGLE:
				shape = new Triangle();
				break;
			default:
				shape = new Circle();
				break;
		}
		return shape;
	}
}
