package designpattern.factory;

/*
 * Simple example of a Triangle Shape and it's functionality
 */
public class Triangle implements Shape {

	@Override
	public void draw() {
		System.out.println("I am a triangle");
	}	
}
