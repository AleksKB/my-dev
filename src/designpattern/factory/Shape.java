package designpattern.factory;

/*
 * Simple Shape interface
 */
public interface Shape {	
	void draw();
}
