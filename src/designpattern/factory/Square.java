package designpattern.factory;

/*
 * Simple example of a Square Shape and it's functionality
 */
public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("I am a square");
	}	
}
