package designpattern.factory;

public enum ShapeType {
	CIRCLE,
	SQUARE,
	TRIANGLE
}
